#!/bin/bash

docker run --rm --name postgres-db -p 5432:5432 -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=P@ssw0rd -e POSTGRES_DB=test -d postgres:12
