package com.ark.control;

import com.ark.model.TShirt;
import com.ark.vm.TShirtSpec;

import javax.inject.Inject;

public class TShirtFactory implements ITShirtFactory {

    @Inject
    private TShirtRepository tshirtRepo;

    @Override
    public TShirt produce(TShirtSpec spec) {
        TShirt shirt = new TShirt();
        shirt.setSize(spec.getSize());
        shirt.setColor(spec.getColor());
        return tshirtRepo.save(shirt);
    }
}
