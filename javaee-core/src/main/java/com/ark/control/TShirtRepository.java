package com.ark.control;

import com.ark.model.TShirt;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Dependent
public class TShirtRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public TShirt save(TShirt shirt) {
        entityManager.persist(shirt);
        return shirt;
    }

    public List<TShirt> getAll() {
        return entityManager
                .createQuery("select s from TShirt s", TShirt.class)
                .getResultList();
    }
}
