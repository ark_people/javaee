package com.ark.control;

import com.ark.model.TShirt;
import com.ark.vm.TShirtSpec;

public interface ITShirtFactory {
    TShirt produce(TShirtSpec spec);
}
