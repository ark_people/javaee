package com.ark.control;

import com.ark.model.TShirt;

import javax.ejb.Lock;
import javax.ejb.Singleton;
import java.util.List;

import static javax.ejb.LockType.READ;

@Singleton
public class ShirtCache {
    private List<TShirt> shirts;

    @Lock(READ)
    public List<TShirt> getAll() {
        return shirts;
    }

    public void cache(List<TShirt> allShirts) {
        this.shirts = allShirts;
    }
}
