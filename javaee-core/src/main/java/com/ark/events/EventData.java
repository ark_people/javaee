package com.ark.events;

public class EventData {
    private Long id;
    private String eventName;

    public EventData() {
    }

    public EventData(Long id, String eventName) {
        this.id = id;
        this.eventName = eventName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public String toString() {
        return "EventData{" +
                "id=" + id +
                ", eventName='" + eventName + '\'' +
                '}';
    }
}
