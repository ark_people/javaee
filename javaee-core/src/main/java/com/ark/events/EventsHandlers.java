package com.ark.events;

import javax.enterprise.event.Observes;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventsHandlers {

    public void handle(@Observes EventData data) {
        Logger.getLogger(EventsHandlers.class.getName()).log(Level.WARNING, data::toString);
    }
}
