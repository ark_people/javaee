package com.ark;

import com.ark.control.ITShirtFactory;
import com.ark.control.ShirtCache;
import com.ark.control.TShirtRepository;
import com.ark.events.EventData;
import com.ark.events.EventProducers;
import com.ark.model.TShirt;
import com.ark.vm.TShirtSpec;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeBoundary {

    @Inject
    private ITShirtFactory tshirtFactory;

    @EJB
    private ShirtCache shirtCache;

    @Inject
    private TShirtRepository tshirtRepository;
    @Inject
    private EventProducers eventProducer;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public TShirt manufactureTShirt(TShirtSpec spec) {
        return tshirtFactory.produce(spec);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TShirt> getAll() {
        List<TShirt> allShirts = shirtCache.getAll();
        if (allShirts == null) {
            allShirts = tshirtRepository.getAll();
            shirtCache.cache(allShirts);
            List<TShirt> finalAllShirts = allShirts;
            Logger.getLogger(EmployeeBoundary.class.getName())
                    .log(Level.WARNING, () -> "Size of shirts list" + finalAllShirts.size());
        }

        return allShirts;
    }

    @POST
    @Path("fire-event")//500
    public Response getValue(EventData eventData) {
        eventProducer.fireEvent(eventData);
        return Response.noContent().build();
    }

    @POST
    @Path("json-data")
    public JsonObject getJson(JsonObject object) {//JSON-P
        Logger.getLogger("MyLogger***************************************")
                .log(Level.WARNING, object.toString());
        JsonValue id = object.getValue("id");
        if (id instanceof JsonNumber) {
            //TODO
        } else {

        }
        return object;
    }
}
