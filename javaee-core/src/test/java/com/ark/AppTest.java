package com.ark;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.OK;
import static org.valid4j.matchers.http.HttpResponseMatchers.hasStatus;

public class AppTest {
    @Test
    public void test() {
        Response response = ClientBuilder.newClient()
                .target("http://localhost:8080/resources/shirt-factory")
                .request()
                .get();

        Assert.assertThat(response, hasStatus(OK));
        System.out.println(response.readEntity(String.class));
    }
}
